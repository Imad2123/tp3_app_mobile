package com.example.tp3;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    SimpleCursorAdapter adapter ;
    ListView list;
    WeatherDbHelper db ;
    public static final String SELECTED_CITY = "SELECTED_CITY";
    private static final int NEW_CITY_ACTIVITY_REQUEST_CODE = 42;
    private static final int CITY_ACTIVITY_REQUEST_CODE = 43;
    private SwipeRefreshLayout mRefreshLayout;
    private Cursor mCursor;

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        list = (ListView)findViewById(R.id.list);
        db = new WeatherDbHelper(getApplicationContext());
        db.populate();
        final Cursor cursor = db.fetchAllCities();
        adapter = new SimpleCursorAdapter(MainActivity.this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[]{WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.COLUMN_COUNTRY},
                new int[]{android.R.id.text1, android.R.id.text2}, 0);
        list.setAdapter(adapter);

        final Intent intent = new Intent(this, CityActivity.class);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor)list.getItemAtPosition(position);
                City city = db.cursorToCity(cursor);
                intent.putExtra("city",city);
                startActivity(intent);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (CITY_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
            City updatedCity = data.getParcelableExtra(MainActivity.SELECTED_CITY);
            db.updateCity(updatedCity);
            mCursor = db.fetchAllCities();
            adapter.changeCursor(mCursor);
            adapter.notifyDataSetChanged();
            System.out.println("City :" + updatedCity.getName() + " Country :" + updatedCity.getCountry());
        }
        if (NEW_CITY_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
            City newCity = data.getParcelableExtra(NewCityActivity.BUNDLE_EXTRA_NEW_CITY);
            db.addCity(newCity);
            mCursor = db.fetchAllCities();
            adapter.changeCursor(mCursor);
            adapter.notifyDataSetChanged();
            System.out.println("City :" + newCity.getName() + " Country :" + newCity.getCountry());

        }
    }


    class UpdateCitiesWheather extends AsyncTask<Object, Integer, List<City>> {
        private List<City> mCityList;

        @Override
        protected void onPreExecute() {
            mCityList = db.getAllCities();
            super.onPreExecute();
        }

        @Override
        protected List<City> doInBackground(Object... objects) {
            for (City city : mCityList) {
                weatherRequest(city);
                db.updateCity(city);
            }
            return mCityList;
        }


        @Override
        protected void onPostExecute(List<City> cities) {
            mCursor = db.fetchAllCities();
            adapter.changeCursor(mCursor);
            adapter.notifyDataSetChanged();
            mRefreshLayout.setRefreshing(false);
            super.onPostExecute(cities);

        }

        private void weatherRequest(City city) {
            HttpURLConnection urlConnection = null;
            try {
                // prepare url
                URL urlToRequest = WebServiceUrl.build(city.getName(), city.getCountry());
                // send a GET request to the serve
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                // read data
                InputStream inputStream = urlConnection.getInputStream();
                JSONResponseHandler jsonHandler = new JSONResponseHandler(city);
                jsonHandler.readJsonStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {

        super.onResume();
        Cursor cursor = db.fetchAllCities();
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();
    }


}
