package com.example.tp3;
import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tp3.City;

public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;
    private City city ;
    public static final String BUNDLE_EXTRA_NEW_CITY = "BUNDLE_EXTRA_NEW_CITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);
        final WeatherDbHelper db = new WeatherDbHelper(getApplicationContext());

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String nom = String.valueOf(textName.getText());
                String pays = String.valueOf(textCountry.getText());
                city = new City(nom,pays);
                db.addCity(city);
                Toast.makeText(NewCityActivity.this,"sauvgardage des données", Toast.LENGTH_LONG).show();
            }
        });
    }


}
