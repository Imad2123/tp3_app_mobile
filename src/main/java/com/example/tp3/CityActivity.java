package com.example.tp3;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.example.tp3.R;
import com.example.tp3.City;
import com.example.tp3.JSONResponseHandler;
import com.example.tp3.WebServiceUrl;

public class CityActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView textCityName, textCountry, textTemperature, textHumdity, textWind, textCloudiness, textLastUpdate;
    private ImageView imageWeatherCondition;
    private City city;
    public static Context c;
    private SwipeRefreshLayout mRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);
        c = getApplicationContext();
        city = getIntent().getParcelableExtra("city");

        textCityName = findViewById(R.id.nameCity);
        textCountry = findViewById(R.id.country);
        textTemperature = findViewById(R.id.editTemperature);
        textHumdity = findViewById(R.id.editHumidity);
        textWind = findViewById(R.id.editWind);
        textCloudiness = findViewById(R.id.editCloudiness);
        textLastUpdate = findViewById(R.id.editLastUpdate);

        imageWeatherCondition = findViewById(R.id.imageView);

        mRefreshLayout = findViewById(R.id.refresh_layout_city);
        mRefreshLayout.setOnRefreshListener(this);
        updateView();

        final Button but = findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (MainActivity.isOnline(CityActivity.this)) {
                    mRefreshLayout.setRefreshing(true);
                    new UpdateCityWheather().execute();

                } else {
                    Snackbar.make(findViewById(R.id.refresh_layout_city), "Vérifiez votre connexion internet", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

            }
        });

    }


    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        System.out.println("back");

        Intent intent = new Intent();
        intent.putExtra(MainActivity.SELECTED_CITY, city);
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }

    private void updateView() {

        textCityName.setText(city.getName());
        textCountry.setText(city.getCountry());
        textTemperature.setText(city.getTemperature() + " °C");
        textHumdity.setText(city.getHumidity() + " %");
        textWind.setText(city.getFullWind());
        textCloudiness.setText(city.getHumidity() + " %");
        textLastUpdate.setText(city.getLastUpdate());

        if (city.getIcon() != null && !city.getIcon().isEmpty()) {
            Log.d(TAG, "icon=" + "icon_" + city.getIcon());
            imageWeatherCondition.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/" + "icon_" + city.getIcon(), null, getPackageName())));
            imageWeatherCondition.setContentDescription(city.getDescription());
        }

    }

    @Override
    public void onRefresh() {
        if (MainActivity.isOnline(CityActivity.this)) {
            new UpdateCityWheather().execute();

        } else {
            Snackbar.make(findViewById(R.id.refresh_layout_city), "Vérifiez votre connexion internet", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }


    }

    class UpdateCityWheather extends AsyncTask<Object, Integer, City> {
        @Override
        protected City doInBackground(Object... objects) {

            HttpURLConnection urlConnection = null;
            try {
                // prepare url
                URL urlToRequest = WebServiceUrl.build(city.getName(), city.getCountry());
                // send a GET request to the serve
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                // read data
                InputStream inputStream = urlConnection.getInputStream();
                JSONResponseHandler jsonHandler = new JSONResponseHandler(city);
                jsonHandler.readJsonStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
            return city;
        }


        @Override
        protected void onPostExecute(City city) {
            updateView();
            mRefreshLayout.setRefreshing(false);
            super.onPostExecute(city);

        }
    }


}
